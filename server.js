const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    cors = require('./helpers/cors'),
    morgan = require('morgan'),
    env = require(`./environments/${process.env.npm_lifecycle_event}.json`),
    { handler } = require('./helpers/error'),
    terminate = require('./helpers/terminate');

if (env.production) { // log, format: 
    process.env.NODE_ENV = 'production';
    app.use(morgan(':method -> :url [:status] :res[content-length] - :response-time ms - :date[clf]', {
        skip: function (req, res) { return res.statusCode < 400 }
    }));
} else {
    process.env.NODE_ENV = 'develop';
    app.use(morgan(':method -> :url [:status] :res[content-length] - :response-time ms - :date[clf]'));
}

app.timeout = 10000;
app.use(cors); // 1er middleware en agregar
app.use(bodyParser.json());
app.use(cookieParser()); // maneja la cookie HttpOnly con jwt
app.use(express.urlencoded({ extended: false }));


require('./routes/main')(app);
app.get('/_health', (req, res) => { // para uptimerobot
    res.status(200).send('ok')
});
app.use((err, req, res, next) => { // dejar de último para que atrape todos los request
    handler(err, res, next);
});

/**
 * @link https://blog.heroku.com/best-practices-nodejs-errors
 * @todo forever =S
 */
const server = app.listen(env.port || process.env.PORT, () => { 
    console.log(`APP -> PORT: ${env.port || process.env.PORT}`);
    console.log('APP -> env.production:', env.production);
    console.log('APP -> process.env.NODE_ENV:', process.env.NODE_ENV);
    console.log('APP -> process.env.npm_lifecycle_event:', process.env.npm_lifecycle_event);
});

const exitHandler = terminate(server, {
    coredump: false,
    timeout: 500
});

process.on('uncaughtException', exitHandler(1, 'Unexpected Error'));
process.on('unhandledRejection', exitHandler(1, 'Unhandled Promise'));
process.on('SIGTERM', exitHandler(0, 'SIGTERM'));
process.on('SIGINT', exitHandler(0, 'SIGINT'));