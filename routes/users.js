const express = require('express'),
    router = express.Router(),
    controller = require('../controllers/users');


router.get('/formData', (req, res, next) => {
    (async () => {
        try {
            res.status(200).json({
                statusCode: 200,
                data: await controller.formData(),
            }).end();
        } catch (e) { 
            next(e); 
        }
    })();
});


router.get('/', (req, res, next) => {
    (async () => {
        try {
            res.status(200).json({
                statusCode: 200,
                data: await controller.read(),
            }).end();
        } catch (e) { 
            next(e); 
        }
    })();
});


router.post('/', (req, res, next) => {
    (async () => {
        try {
            res.status(200).json({
                statusCode: 200,
                data: await controller.create(req.body),
            }).end();
        } catch (e) { 
            next(e); 
        }
    })();
});


router.put('/:key', (req, res, next) => {
    (async () => {
        try {
            res.status(200).json({
                statusCode: 200,
                data: await controller.update(req.params.key, req.body),
            }).end();
        } catch (e) { 
            next(e); 
        }
    })();
});


router.delete('/:key', (req, res, next) => {
    (async () => {
        try {
            res.status(200).json({
                statusCode: 200,
                data: await controller.deleto(req.params.key, req.body),
            }).end();
        } catch (e) { 
            next(e); 
        }
    })();
});



module.exports = router;
