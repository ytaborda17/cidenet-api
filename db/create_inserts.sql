CREATE TABLE "identificacion_tipo" (
	"id" INTEGER PRIMARY KEY,
	"nombre" TEXT NOT NULL
);

CREATE TABLE "area" (
	"id" INTEGER PRIMARY KEY,
	"nombre" TEXT NOT NULL
);

CREATE TABLE "pais" (
	"id" INTEGER PRIMARY KEY,
	"nombre" TEXT NOT NULL
);



CREATE TABLE "usuario" (
	"id" INTEGER PRIMARY KEY,
	"apellido1" TEXT NOT NULL,
	"apellido2" TEXT NOT NULL,
	"nombre1" TEXT NOT NULL,
	"nombre2" TEXT NOT NULL,
	"email" TEXT NOT NULL UNIQUE,
	"pais" INTEGER NOT NULL,
	"identificacion_tipo" INTEGER NOT NULL,
	"identificacion" TEXT NOT NULL,	
	"area" INTEGER NOT NULL,
	"ingreso" TEXT NOT NULL,
	"registro" TEXT,
	"estado" INTEGER NULL DEFAULT '1',
	"editado" TEXT,
	UNIQUE(identificacion_tipo,identificacion)
);


INSERT INTO "area" ("id", "nombre") VALUES
	(1, 'Administración'),
	(2, 'Financiera'),
	(3, 'Compras'),
	(4, 'Infraestructura'),
	(5, 'Operación'),
	(6, 'Talento Humano'),
	(7, 'Servicios Varios');


INSERT INTO "pais" ("id", "nombre") VALUES
	(1, 'Colombia'),
	(2, 'Estados Unidos');


INSERT INTO "identificacion_tipo" ("id", "nombre") VALUES
	(1, 'Cédula de Ciudadanía'),
	(2, 'Cédula de Extranjería'),
	(3, 'Pasaporte'),
	(4, 'Permiso Especial');