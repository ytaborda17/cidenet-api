const path = require('path'),
knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, `../db/${process.env.npm_lifecycle_event}.db`)
    },
    useNullAsDefault: true,
    // debug: true,
    pool: { min: 0, max: 7 }
});


/**
 * @param {object} data 
 * @returns {number} id del registro agregado
 */
async function create(data) {
    return new Promise((resolve, reject) => {
        knex('usuario').insert(data).then(usuarioId => {
            if ((!usuarioId || usuarioId.length == 0)) {
                reject(`No se pudo guardar "usuario"`);
            } else {
                resolve(usuarioId);
            }
        }).catch(err => {
            reject(err);
        });
    }).catch(error => {
        throw error;
    });
}


/**
 * @param {object} conditions Ej: {'id': 17}
 * @returns {object[]} listado de usuarios
 */
function read(conditions) {
    return new Promise((resolve, reject) => {
        const query = knex
        .select('usuario.*', 'pais.nombre as pais_nombre','area.nombre as area_nombre','identificacion_tipo.nombre as identificacion_tipo_nombre', )
        .from('usuario')
        .join('pais', {'usuario.pais': 'pais.id'})
        .join('area', {'usuario.area': 'area.id'})
        .join('identificacion_tipo', {'usuario.identificacion_tipo': 'identificacion_tipo.id'});
        if (conditions) {
            query.where(conditions);
        }
        query.then(rows => {
            resolve(rows);
        })
        .catch(err => {
            reject(err);
        });
    }).catch(e => {
        throw e;
    });
}


/**
 * @param {object} conditions Ej: {'id': 17}
 * @param {object} data datos a actualizar Ej: {'nombre1': 'Nuevo nombre'}
 * @returns {number} cantidad de registros actualizados
 */
async function update(conditions, data) {
    return new Promise((resolve, reject) => {
        knex('usuario').update(data).where(conditions).then(updated => {
            if (!updated || updated.length == 0) {
                throw 'No se pudo actualizar "usuario"';
            } else {
                resolve(updated);
            }
        }).catch(err => {
            reject(err);
        });
    }).catch(error => {
        throw error;
    });
}


/**
 * @param {object} conditions Ej: {'id': 17}
 * @returns {number} cantidad de registros actualizados
 */
function deleto(conditions) {
    return new Promise((resolve, reject) => {
        knex('usuario').del().where(conditions).then(deleted => {
            if (!deleted || deleted.length == 0) {
                throw 'No se pudo borrar "usuario"';
            } else {
                resolve(deleted);
            }
        }).catch(err => {
            reject(err);
        });
    }).catch(error => {
        throw error;
    });
}



module.exports = {
    create,
    read, 
    update,
    deleto,
};