const path = require('path'),
knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, `../db/${process.env.npm_lifecycle_event}.db`)
    },
    useNullAsDefault: true,
    // debug: true,
    pool: { min: 0, max: 7 }
});

/**
 * @returns {object[]} listado de paises
 */
function read() {
    return new Promise((resolve, reject) => {
        knex.select('*').from('identificacion_tipo').then(rows => {
            resolve(rows);
        })
        .catch(err => {
            reject(err);
        });
    }).catch(e => {
        throw e;
    });
}

module.exports = {
    read, 
};