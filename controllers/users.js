const { ErrorHandler } = require('../helpers/error'),
    model = {
        usuario: require('../models/usuario'),
        pais: require('../models/pais'),
        identificacion_tipo: require('../models/identificacion_tipo'),
        area: require('../models/area'),
    };

async function formData() {
    try {
        return {
            pais: await model.pais.read(),
            identificacion_tipo: await model.identificacion_tipo.read(),
            area: await model.area.read(),
        };
    } catch (error) {
        throw new ErrorHandler(500, 'Se ha producido un error', error);
    }
}

async function read() {
    try {
        return await model.usuario.read();
    } catch (error) {
        throw new ErrorHandler(500, 'Se ha producido un error', error);
    }
}

async function create(body) {
    try {
        const domain = body.pais == 1 ? 'cidenet.com.co' : 'cidenet.com.us',
            existe_user_identificacion = await model.usuario.read({
                'identificacion': body.identificacion,
                'identificacion_tipo': body.identificacion_tipo,
            });

        if (existe_user_identificacion.length > 0) {
            throw 'identificacion';
        }

        let mail_address = `${body.nombre1.replace(/ /g,'')}.${body.apellido1.replace(/ /g,'')}`.toLowerCase();
        
        body.estado = true;
        body.registro = new Date().toISOString();
        body.email = `${mail_address}@${domain}`;
        const emails_repetidos = await model.usuario.read({
            'nombre1': body.nombre1,
            'apellido1': body.apellido1,
        });

        if (emails_repetidos.length > 0) {
            body.email = `${mail_address}.${emails_repetidos.length}@${domain}`;
        }

        return await model.usuario.create(body);
        
    } catch (error) {
        if (error == 'identificacion') {
            throw new ErrorHandler(409, 'La identificación ingresada ya existe con otro usuario');
        } else {
            throw new ErrorHandler(500, 'Se ha producido un error inesperado', error);
        }
    }
}


async function update(key, body) {
    try {
        const current_user = await model.usuario.read({'usuario.id': key});
        body.editado = new Date().toISOString();

        if (body.nombre1 || body.apellido1 || body.pais) { 
            const domain = (body.pais || current_user[0].pais) == 1 ? 'cidenet.com.co' : 'cidenet.com.us';
            let mail_address = `${(body.nombre1 || current_user[0].nombre1).replace(/ /g,'')}.${(body.apellido1 || current_user[0].apellido1).replace(/ /g,'')}`.toLowerCase();
            
            if (mail_address.length > 300) {
                mail_address = mail_address.substr(0,299);
            }
    
            body.email = `${mail_address}@${domain}`;
            const emails_repetidos = await model.usuario.read({
                'nombre1': body.nombre1 || current_user[0].nombre1,
                'apellido1': body.apellido1 || current_user[0].apellido1,
            });
    
            if (emails_repetidos.length > 0) {
                body.email = `${mail_address}.${emails_repetidos.length}@${domain}`;
            }
        }
        return await model.usuario.update({'id': key}, body);
    } catch (error) {
        throw new ErrorHandler(500, 'Se ha producido un error', error);
    }
}


async function deleto(key) {
    try {
        return await model.fb.deleto('users', key);
    } catch (error) {
        throw new ErrorHandler(500, 'Se ha producido un error', error);
    }
}



module.exports = {
    formData,
    read,
    create,
    update,
    deleto
};