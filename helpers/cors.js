const env = require(`../environments/${process.env.npm_lifecycle_event}.json`);

module.exports = (req, res, next) => {

    
    if ((process.env.allowOrigin && process.env.allowOrigin.includes(req.headers.origin)) || env.security.allowOrigin.includes(req.headers.origin)) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }

    next();
};
