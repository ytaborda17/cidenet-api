class ErrorHandler extends Error {
    constructor(statusCode, message, error = null) {
        super();
        this.statusCode = statusCode;
        this.message = message;
        if (error && error !== null) {
            console.log('ERR -> ', error);
        }
    }
}

module.exports = {
    ErrorHandler,
    handler: (err, res, next) => {
        const { statusCode, message } = err;
        res.status(statusCode || 500).json({
            status: false,
            statusCode: statusCode || 500,
            message: message,
        }).end();
    }
};
